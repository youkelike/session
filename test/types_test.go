package session

import (
	"html/template"
	"net/http"
	"testing"
	"time"

	"gitee.com/youkelike/session"
	"gitee.com/youkelike/session/cookie"
	"gitee.com/youkelike/session/memory"
	"gitee.com/youkelike/web"
	"github.com/stretchr/testify/require"
)

func TestSession(t *testing.T) {
	// var m session.Manager
	m := &session.Manager{
		Store:      memory.NewStore(time.Minute * 15),
		Propagator: cookie.NewPropagator(),
	}

	tpl, err := template.ParseGlob("tpls/*.gohtml")
	require.NoError(t, err)
	engine := &web.GoTemplateEngine{
		T: tpl,
	}

	// 添加检查刷新 session 的中间件
	server := web.NewHTTPServer(web.ServerWithMiddleware(func(next web.HandlerFunc) web.HandlerFunc {
		return func(ctx *web.Context) {
			if ctx.Req.URL.Path == "/login" {
				next(ctx)
				return
			}

			// 先检查是否有 session
			_, err := m.GetSession(ctx)
			if err != nil {
				ctx.RespStatusCode = http.StatusUnauthorized
				ctx.RespData = []byte("请重新登陆")
				return
			}
			// 再刷新 session
			_ = m.RefreshSession(ctx)

			next(ctx)
		}
	}), web.ServerWithTemplateEngine(engine))

	server.Post("/login", func(ctx *web.Context) {
		// 这里写登陆校验逻辑
		// ...

		// 初始化 session 并把 session 注入到 cookie
		sess, err := m.InitSession(ctx)
		if err != nil {
			ctx.RespStatusCode = http.StatusInternalServerError
			ctx.RespData = []byte("登陆失败")
			return
		}

		// 存储数据到 session 对象中
		err = sess.Set(ctx.Req.Context(), "username", "bob")
		if err != nil {
			ctx.RespStatusCode = http.StatusInternalServerError
			ctx.RespData = []byte("登陆失败")
			return
		}

		ctx.RespStatusCode = http.StatusOK
		ctx.RespData = []byte("登陆成功")
	})

	server.Get("/logout", func(ctx *web.Context) {
		_ = ctx.Render("logout.gohtml", nil)
	})
	server.Post("/logout", func(ctx *web.Context) {
		err := m.RemoveSession(ctx)
		if err != nil {
			ctx.RespStatusCode = http.StatusInternalServerError
			ctx.RespData = []byte("退出登陆失败")
			return
		}

		ctx.RespStatusCode = http.StatusOK
		ctx.RespData = []byte("已退出登陆")
	})

	server.Get("/user", func(ctx *web.Context) {
		sess, _ := m.GetSession(ctx)

		val, err := sess.Get(ctx.Req.Context(), "username")
		if err != nil {
			ctx.RespStatusCode = http.StatusInternalServerError
			ctx.RespData = []byte("获取信息失败")
			return
		}

		ctx.RespStatusCode = http.StatusOK
		ctx.RespData = []byte(val.(string))
	})

	server.Get("/login", func(ctx *web.Context) {
		_ = ctx.Render("login.gohtml", nil)
	})

	server.Start(":8081")
}
