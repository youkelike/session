package cookie

import "net/http"

type Propagator struct {
	cookieName   string
	cookieOption func(*http.Cookie)
}

func NewPropagator(opts ...PropagatorOption) *Propagator {
	p := &Propagator{
		cookieName:   "sessid",
		cookieOption: func(c *http.Cookie) {},
	}

	for _, o := range opts {
		o(p)
	}

	return p
}

type PropagatorOption func(*Propagator)

func WithCookieName(name string) PropagatorOption {
	return func(p *Propagator) {
		p.cookieName = name
	}
}

func WithCookieOption(o func(*http.Cookie)) PropagatorOption {
	return func(p *Propagator) {
		p.cookieOption = o
	}
}

func (p *Propagator) Extract(req *http.Request) (string, error) {
	c, err := req.Cookie(p.cookieName)
	if err != nil {
		return "", err
	}
	return c.Value, nil
}

func (p *Propagator) Inject(id string, writer http.ResponseWriter) error {
	c := &http.Cookie{
		Name:  p.cookieName,
		Value: id,
	}
	p.cookieOption(c)
	http.SetCookie(writer, c)
	return nil
}

func (p *Propagator) Remove(writer http.ResponseWriter) error {
	http.SetCookie(writer, &http.Cookie{
		Name:   p.cookieName,
		MaxAge: -1,
	})
	return nil
}
